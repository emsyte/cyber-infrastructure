# Instructions on how to produce nice network topology graphs

## 1. Produce a Graphwiz dot graph

terraform graph > ../diagrams/aws.dot

## 2. Convert the dot graph into a GML file

cd ../diagrams
gv2gml aws.dot > aws.gml

## 3. Import into yED and optimize the layout

open aws.gml

* Tools -> Fit Node to Label -> OK
* Structure View -> Select All Nodes -> Properties View -> Shape: Rectangle
* Layout -> BPMN -> OK
* Tools -> Geometric Transformations -> Mirror on Y-Axis
