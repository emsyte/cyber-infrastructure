# Tailored Labs Kubernetes Infrastructure

## Installation

```
# 1. Setup the Azure CLI tool
# 2. Create a service principal for use with the CLI
# 3. Enable AKS
./pre_terraform.sh

# Build the k8s cluster
terraform apply

# 1. Setup kubectl
# 2. Setup helm
./post_terraform.sh
```