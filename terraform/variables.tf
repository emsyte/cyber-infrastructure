variable "location" {
  type = "string"
  default = "East US"
}

variable "node_disk_size" {
  type = "string"
  default = "30"
}

variable "node_size" {
  type = "string"
  default = "Standard_B2ms"
}

variable "node_count" {
  type = "string"
  default = "1"
}

variable "client_id" {
  type = "string"
  default = "59c5f0ba-6030-489e-8bf1-b0a7cac4dc42"
}

variable "client_secret" {
  type = "string"
  default = "18a8ea08-19c0-4004-8133-fc6c143e7d5b"
}