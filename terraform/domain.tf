resource "azurerm_dns_zone" "tailoredlabs" {
  name                = "tailoredlabs.org"
  resource_group_name = "${azurerm_resource_group.tailoredlabs.name}"
}

resource "azurerm_dns_mx_record" "zoho" {
  name = "@"
  zone_name = "${azurerm_dns_zone.tailoredlabs.name}"
  resource_group_name = "${azurerm_resource_group.tailoredlabs.name}"
  ttl = "300"

  record {
    preference = 10
    exchange = "mx.zoho.com"
  }

  record {
    preference = 20
    exchange = "mx2.zoho.com"
  }

  record {
    preference = 50
    exchange = "mx3.zoho.com"
  }
}