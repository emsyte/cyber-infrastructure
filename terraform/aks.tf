resource "azurerm_resource_group" "tailoredlabs" {
  name     = "tailoredlabs"
  location = "${var.location}"
}

resource "azurerm_kubernetes_cluster" "tailoredlabs" {
  name                = "tailoredlabs"
  location            = "${azurerm_resource_group.tailoredlabs.location}"
  resource_group_name = "${azurerm_resource_group.tailoredlabs.name}"
  dns_prefix          = "k8s"
  kubernetes_version  = "1.9.6"

  linux_profile {
  admin_username = "k8s"

    ssh_key {
      key_data = "${file("keys/tailoredlabs.pub")}"
    }
  }

  agent_pool_profile {
    name            = "default"
    count           = "${var.node_count}"
    vm_size         = "${var.node_size}"
    os_type         = "Linux"
    os_disk_size_gb = "${var.node_disk_size}"
  }

  service_principal {
    client_id     = "${var.client_id}"
    client_secret = "${var.client_secret}"
  }

  tags {
    Environment = "Production"
  }
}