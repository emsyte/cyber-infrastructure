#!/usr/bin/env bash

source _utils.sh

function login() {
  log " -> Logging in to Azure"
  az login
}

function create_service_principal() {
  log " -> Creating a contributor service principal to login with"
  export service_principal="$(az ad sp create-for-rbac --role='Contributor' --scopes='/subscriptions/${subscription_id}')"
  export client_id="$(echo $service_principal | jq -r '.appId')"
  export client_secret="$(echo $service_principal | jq -r '.password')"
  export tenant="$(echo $service_principal | jq -r '.tenant')"

  log "\n${bold}CLIENT ID${normal}:\t${client_id}"
  log "${bold}CLIENT SECRET${normal}:\t${client_secret}"
  log "${bold}TENANT${normal}:\t${tenant}\n"
  az login --service-principal -u ${client_id} -p ${client_secret} --tenant ${tenant_id}
}

function get_account() {
  export account="$(az account list)"
  if [ "$!" != "0" ]; then return 1; fi
}

log " -> Retrieving the subscription ID"
get_account

while [ -z ${account+x} ]; do
    # Check that accounts came back with sensible data
    echo >&2 "Please try again - your login didn't work for some reason."
    login
    get_account
done

subscription_id="$(echo $account | jq -r '.[0].id')"

log "\n${bold}SUBSCRIPTION ID${normal}:\t${subscription_id}\n"

log " -> Setting the subscription on your account"
az account set --subscription="${subscription_id}"

create_service_principal

log " -> Setting up azcli to support Azure AKS"
az provider register -n Microsoft.ContainerService
