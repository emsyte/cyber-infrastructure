#!/usr/bin/env bash

source _utils.sh

resource_group="tailoredlabs"
cluster_name="tailoredlabs"

function wait_for_tiller() {
    while [ "$(kubectl -n kube-system get pods | grep tiller | grep Running | grep '1/1')" == "" ]; do
        log ":: Waiting for tiller to come up..."
        sleep 2
    done
}

function wait_for_traefik() {
  while [ "$(kubectl -n kube-system get pods | grep traefik | grep Running | grep '1/1')" == "" ]; do
    log ":: Waiting for traefik to come up..."
    sleep 2
  done
  while [ "$(kubectl get svc traefik -n kube-system --no-headers | awk '{print $4}')" == "<pending>" ]; do
    log ":: Waiting for traefik to get an external IP..."
    sleep 10
  done
}

function wait_for_service_catalog() {
    while [ "$(kubectl -n catalog get pods | grep catalog-apiserver | grep Running | grep '2/2')" == "" ]; do
        log ":: Waiting for catalog API server to come up..."
        sleep 2
    done
    while [ "$(kubectl -n catalog get pods | grep catalog-controller-manager | grep Running | grep '1/1')" == "" ]; do
        log ":: Waiting for catalog API server to come up..."
        sleep 2
    done
}

function wait_for_osba() {
    while [ "$(svcat get broker osba | grep osba | awk '{print $3}')" != "Ready"]; do
        log ":: Waiting for OSBA broker to be ready..."
        sleep 5
    done
}

log " -> Retrieving k8s credentials from Azure"

credentials="$(az aks get-credentials --resource-group ${resource_group} --name ${cluster_name})"
if [[ $credentials != *"Merged"* ]]; then
    log " Error: the credentials weren't fetched correctly:"
    echo -e $credentials
fi

while [ "$(kubectl get nodes -o name | grep 'node/aks')" == "" ]; do
    log "Waiting for k8s nodes to come up... ($(kubectl get nodes -o name))"
    sleep 5
done

log " -> Setting up helm"
helm init
wait_for_tiller
log " -> Ensuring that the Service Account Token is mounted correctly (see https://github.com/kubernetes/helm/issues/2464#issuecomment-381229646 for more info):"
kubectl -n kube-system patch deployment tiller-deploy -p '{"spec": {"template": {"spec": {"automountServiceAccountToken": true}}}}'
wait_for_tiller

log " -> Installing the Traefik loadbalancer"
log "${bold}NB:${normal} This normally takes a few minutes as we have to wait for an IP from Azure"
helm install stable/traefik --name traefik --namespace kube-system --values helm_overrides/traefik.yaml
wait_for_traefik


log " -> Installing Service Broker catalog"

RBAC="$(az ad sp create-for-rbac -o json)"
AZURE_SUBSCRIPTION_ID="$(az account show --query id --out tsv)"
AZURE_TENANT_ID="$(echo ${RBAC} | jq -r .tenant)"
AZURE_CLIENT_ID="$(echo ${RBAC} | jq -r .appId)"
AZURE_CLIENT_SECRET="$(echo ${RBAC} | jq -r .password)"
AZURE_SP_NAME="$(echo ${RBAC} | jq -r .name)"

helm repo add svc-cat https://svc-catalog-charts.storage.googleapis.com
helm install svc-cat/catalog \
     --name catalog \
     --namespace catalog \
     --set rbacEnable=false

wait_for_service_catalog

log " -> Installing Azure Service Broker"

helm repo add azure https://kubernetescharts.blob.core.windows.net/azure
helm install azure/open-service-broker-azure \
     --name osba \
     --namespace osba \
     --set azure.subscriptionId=${AZURE_SUBSCRIPTION_ID} \
     --set azure.tenantId=${AZURE_TENANT_ID} \
     --set azure.clientId=${AZURE_CLIENT_ID} \
     --set azure.clientSecret=${AZURE_CLIENT_SECRET}

wait_for_osba

log " -> Building configuration for pulling from Gitlab's Docker registry"
DOCKER_REGISTRY_SERVER=https://registry.gitlab.com

echo -n "Enter Gitlab username : " 
read DOCKER_USER

echo -n "Enter Gitlab email : "
read DOCKER_EMAIL

echo -n "Enter Gitlab password : "
read -s DOCKER_PASSWORD

log " -> Creating..."

kubectl create secret docker-registry gitlab-registry \
  --docker-server=$DOCKER_REGISTRY_SERVER \
  --docker-username=$DOCKER_USER \
  --docker-password=$DOCKER_PASSWORD \
  --docker-email=$DOCKER_EMAIL

log " -> Building configuration for Gitlab CI runner"
log "Gitlab has a Kubernetes integration which we want to take advantage of."
log "We will produce some config data which we can feed into gitlab in order for gitlab to connect to our cluster"

kubectl create -f kubectl/gitlab-service-account.yaml
tokenfile="$(kubectl get serviceaccounts/gitlab -o json | jq -r '.secrets[0].name')"
kube_config="$(cat ${HOME}/.kube/config | python -c 'import sys, yaml, json; json.dump(yaml.load(sys.stdin), sys.stdout, indent=4)')"
api_url="$(echo ${kube_config} | jq -rc '.clusters[] | select( .name | contains("tailoredlabs") ) | .cluster.server')"
token="$(kubectl get secret/${tokenfile} -o json | jq -r '.data.token' | base64 -d)"
cert="$(kubectl get secret/${tokenfile} -o json | jq -r '.data."ca.crt"' | base64 -d)"

log " -> Results:"
log "${bold}cluster name:${normal}\t$(az aks list | jq -r '.[0].name')"
log "${bold}API URL:${normal}\t${api_url}"
log "${bold}CA Certificate:${normal}\n\n${cert}\n"
log "${bold}Token:${normal}\t${token}"

log " -> All done! 🍺"
