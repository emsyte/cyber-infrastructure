#!/usr/bin/env bash

# Add some colour to your life
export bold=$(tput bold)
export normal=$(tput sgr0)

function y_to_continue() {
    read -n1 -r -p "${bold}Press y to continue...${normal}" key
    if [ "$key" = 'y' ]; then
        echo ""
        return
    else
        y_to_continue
    fi
}

function log() {
    echo -e $@
}

# Check all necessary dependencies are installed:
for dep in svcat awk grep az jq helm kubectl virtctl terraform; do
    command -v ${dep} >/dev/null 2>&1 || { echo >&2 "You must have ${dep} on your \$PATH before you can use this script. Aborting."; exit 1; }
done
